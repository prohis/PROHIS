Os recomiendo a todos descargar la siguiente aplicación:
https://www.sourcetreeapp.com/

Es un cliente de git, de forma que no hay que introducir los comandos en un
terminal, y podeis clonar el repositorio y hacer commits/push y demás de forma
muy fácil.